#! /usr/bin/python3
# Copyright © 2016 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
# Definitely not my finest work, but it does its job

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import argparse
import os, shutil, subprocess, traceback
from typing import Iterable

kAndroidBaseDir = 'platform/android/assets/res'
kIosBaseDir = 'platform/ios/assets/images'
kOutDirBase = ''
kTmpDirBase = ''


def gen_ms_icon(source: str, dest: str, size: int) -> None:
    tmp_f = os.path.join(kTmpDirBase, 'win.png')
    gen_icon(source, tmp_f, size)
    os.makedirs(os.path.dirname(dest), exist_ok=True)
    subprocess.check_call([ 'icotool', '-c', '-o', dest, tmp_f ])
    os.remove(tmp_f)


def gen_icon(source: str, dest: str, size: int) -> None:
    s = str(size)
    tmp_f = os.path.join(kTmpDirBase, 'img.png')
    subprocess.check_call([ 'convert', '-resize', s+'x'+s, '-quality', '100', source, tmp_f ])
    os.makedirs(os.path.dirname(dest), exist_ok=True)
    # Brief empirical testing indicates that method 7 is optimal in most cases
    subprocess.check_call([ 'pngcrush', '-m', '7', tmp_f, dest ])
    os.remove(tmp_f)


def gen_windows_icons(source_icon: str) -> None:
    out_dir = os.path.join(kOutDirBase, 'platform/windows/assets')
    gen_ms_icon(source_icon, os.path.join(out_dir, 'skaa_icon.ico'), 128)


def gen_android_icons(source_icon: str) -> None:
    out_dir = os.path.join(kOutDirBase, kAndroidBaseDir)
    for size,text in [ (36, 'ldpi'), (48, 'mdpi'), (72, 'hdpi'), (96, 'xhdpi'), (144, 'xxhdpi'), (192, 'xxxhdpi') ]:
        out_substr = 'drawable-{}/icon.png'.format(text)
        gen_icon(source_icon, os.path.join(out_dir, out_substr), size)


def gen_ios_icons(source_icon: str) -> None:
    out_dir = os.path.join(kOutDirBase, kIosBaseDir, 'icons')
    for size in [ 29, 40, 50, 57, 72, 76, 87 ]:
        name = 'Icon{0}x{0}.png'.format(size)
        gen_icon(source_icon, os.path.join(out_dir, name), size)
    for size in [ 58, 80, 100, 114, 120, 144 ]:
        name = 'Icon{0}x{0}@2x.png'.format(int(size/2))
        gen_icon(source_icon, os.path.join(out_dir, name), size)
    for size in [ 120, 180 ]:
        name = 'Icon{0}x{0}@3x.png'.format(int(size/3))
        gen_icon(source_icon, os.path.join(out_dir, name), size)


def gen_mac_icons(source_icon: str) -> None:
    kTempOutDir = os.path.join(kTmpDirBase, 'mac.iconset')
    out_dir = os.path.join(kOutDirBase, 'platform/mac/assets')
    try:
        os.makedirs(kTempOutDir)
    except:
        shutil.rmtree(kTempOutDir)
        os.makedirs(kTempOutDir)

    for size in [ 16, 32, 128, 256, 512 ]:
        size_s = str(size)
        s_base = [ 'icon_', size_s, 'x', size_s, '.png' ]
        out_name = ''.join(s_base)
        s_base.insert(-1, '@2x')
        out_name_2x = ''.join(s_base)
        gen_icon(source_icon, os.path.join(kTempOutDir, out_name), size)
        gen_icon(source_icon, os.path.join(kTempOutDir, out_name_2x), int(2*size))

    subprocess.check_call(['iconutil', '--convert', 'icns', kTempOutDir])
    shutil.rmtree(kTempOutDir)
    shutil.move(os.path.join(kTmpDirBase, 'mac.icns'), os.path.join(out_dir, 'skaa_icons.icns'))


def copy_android_splash(source_dir: str) -> None:
    out_dir = os.path.join(kOutDirBase, kAndroidBaseDir)
    for size,text in [ ('200x320', 'ldpi'), ('320x480', 'mdpi'), ('480x800', 'hdpi'), ('720x1280', 'xhdpi'), ('960x1600', 'xxhdpi'), ('1280x1920', 'xxxhdpi') ]:
        out_substr = 'drawable-{}/launch.png'.format(text)
        in_f = os.path.join(source_dir, 'launch_{}.png'.format(size))
        shutil.copy(in_f, os.path.join(out_dir, out_substr))


def copy_ios_splash(source_dir: str) -> None:
    out_dir = os.path.join(kOutDirBase, kIosBaseDir, 'launch')
    for size in [ '320x480', '640x960', '640x1136', '750x1334', '768x1024', '1242x2208', '1536x2048', '1080x1920' ]:
        in_f = os.path.join(source_dir, 'launch_{}.png'.format(size))
        out_f = os.path.join(out_dir, 'Launch_{}.png'.format(size))
        shutil.copy(in_f, out_f)


def check_argv(argv: argparse.Namespace, args: Iterable) -> None:
    argv_dict = vars(argv)
    missing_args = []
    for arg in args:
        if argv_dict[arg] is None:
            missing_args.append(arg)
    if len(missing_args) > 0:
        raise Exception('The following required arguments were not specified: {}'.format(missing_args))


def gen_icons(argv: argparse.Namespace) -> None:
    check_argv(argv, [ 'master_icon' ])
    common_out_dir = os.path.join(kOutDirBase, 'common/assets/images')
    gen_icon(argv.master_icon, os.path.join(common_out_dir, 'skaa_icon.png'), 256)
    gen_windows_icons(argv.master_icon)
    gen_android_icons(argv.master_icon)
    gen_ios_icons(argv.master_icon)
    gen_mac_icons(argv.master_icon)


def copy_logo(argv: argparse.Namespace) -> None:
    check_argv(argv, [ 'client_logo' ])
    common_out_dir = os.path.join(argv.out_dir, 'common/assets/images')
    os.makedirs(common_out_dir, exist_ok=True)
    shutil.copy(argv.client_logo, common_out_dir)


def copy_splash(argv: argparse.Namespace) -> None:
    check_argv(argv, [ 'master_splash_dir' ])
    copy_android_splash(argv.master_splash_dir)
    copy_ios_splash(argv.master_splash_dir)


kCommands = {
    'icons': gen_icons,
    'logo': copy_logo,
    'splash': copy_splash,
}


def parse_argv() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('--out-dir', required=True, default=None, type=str)
    parser.add_argument('--temp-dir', required=False, default='/tmp', type=str)
    parser.add_argument('--master-icon', required=False, default=None, type=str)
    parser.add_argument('--master-splash-dir', required=False, default=None, type=str)
    parser.add_argument('--client-logo', required=False, default=None, type=str)
    parser.add_argument('command', default=None, nargs='+', choices=kCommands.keys())
    return parser.parse_args()


if __name__ == '__main__':
    argv = parse_argv()
    kOutDirBase = argv.out_dir
    kTmpDirBase = argv.temp_dir
    for cmd in argv.command:
        try:
            kCommands[cmd](argv)
        except:
            traceback.print_exc()
            print('Command "{}" failed!'.format(cmd))
